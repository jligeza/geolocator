#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.properties import StringProperty, ObjectProperty
from kivy.clock import mainthread
from kivy.app import App

from kivymd.backgroundcolorbehavior import BackgroundColorBehavior
from kivymd.theming import ThemableBehavior
from kivymd.elevationbehavior import ElevationBehavior
from kivymd.ripplebehavior import RectangularRippleBehavior
from kivy.uix.behaviors import ButtonBehavior


class TilesBox(BoxLayout):

    _first_tile_added = False

    def __init__(self, **kwargs):
        super(TilesBox, self).__init__(**kwargs)
        self.bind_to_screen_manager()

    @mainthread
    def bind_to_screen_manager(self):
        app = App.get_running_app()
        sm = app.root.screen_manager
        sm.bind(current=self.on_screen_swap)

    def on_screen_swap(self, obj, screen_name):
        for tile in self.children:
            if tile.screen == screen_name:
                tile.opacity = 1
            else:
                tile.opacity = 0.5

    def add_widget(self, tile, index=0):
        super(TilesBox, self).add_widget(tile, index)
        self._select_first_tile(tile)

    def _select_first_tile(self, tile):
        if self._first_tile_added is True:
            tile.opacity = 0.5
        else:
            tile.opacity = 1
        self._first_tile_added = True


class Tile(ButtonBehavior, RectangularRippleBehavior, RelativeLayout):

    app = App.get_running_app()
    icon = StringProperty()
    text = StringProperty()
    screen = StringProperty()


class MyToolbar(ThemableBehavior, ElevationBehavior,
                BackgroundColorBehavior, BoxLayout):

    tiles_box = ObjectProperty()

    def toggle_tiles_selection(self):
        self.tiles_box.toggle_tiles_selection()
