#!/usr/bin/env python
# -*- coding: utf-8 -*-
__all__ = ['MyToolbar']
from .main import MyToolbar


def load_gui():
    from kivy.lang import Builder
    import os
    file_path = os.path.dirname(os.path.realpath(__file__))
    Builder.load_file(file_path + '/gui.kv')

load_gui()
