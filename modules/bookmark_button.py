#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.lang import Builder
from kivy.properties import (
    ObjectProperty,
    BooleanProperty,
    StringProperty
)
from kivy.clock import mainthread

from kivymd.button import MDIconButton
import kivymd.snackbar as Snackbar

from .bookmarks import facade as bookmarks_facade
from .utils import SimpleDialogWithInput
from .database import Bookmark

from tinydb import Query

Builder.load_string('''
<BookmarkButton>:
    size_hint: None, None
    height: input_height
    width: input_height
    disabled: False if self.focused_location else True
    opacity: 1 if self.focused_location else 0
''')


class BookmarkButton(MDIconButton):
    '''
    Adds passed location to bookmarks if it isn't already there.

    Public methods:
        * focus_location

    Public events:
        * on_dialog_confirm
        * on_dialog_cancel
    '''

    icon = StringProperty('bookmark-outline')
    checked = BooleanProperty(False)

    # google location object - address and coords
    focused_location = ObjectProperty()

    def __init__(self, **kwargs):
        super(BookmarkButton, self).__init__(**kwargs)
        self.register_event_type('on_dialog_confirm')
        self.register_event_type('on_dialog_cancel')
        self._bind_properties()

    def on_dialog_confirm(self):
        pass

    def on_dialog_cancel(self):
        pass

    @mainthread
    def _bind_properties(self):
        bookmarks_facade.bind(
            on_bookmarks_deleted=self._check_deleted_bookmarks)

    def _check_deleted_bookmarks(self, obj, bookmarks):
        if not self.focused_location:
            return

        for bookmark in bookmarks:
            if bookmark.address == self.focused_location['address']:
                self._uncheck()

    def focus_location(self, location, custom_pos=None):
        '''
        Focuses the location, allowing it to be either deleted
        or saved in bookmarks database, on the button press.

        Location must be an object returned by the geopy's google query.

        If set `custom_pos`, coordinates attached to the location object
        will be ignored in favor of the custom coordinates.

        If it already saved, then check the button, else uncheck.
        '''
        location = self._location_to_dict(location)

        if custom_pos:
            location['latitude'] = custom_pos[0]
            location['longitude'] = custom_pos[1]

        self.focused_location = location
        if bookmarks_facade.already_in_table(location['address']):
            self._check()
        else:
            self._uncheck()

    def _location_to_dict(self, location):
        '''Location pools can't be modified, so have to convert to dict.'''
        return {
            'address': location.address,
            'latitude': location.latitude,
            'longitude': location.longitude
        }

    def _check(self):
        self.checked = True
        self.icon = 'bookmark'

    def _uncheck(self):
        self.checked = False
        self.icon = 'bookmark-outline'

    def on_press(self):
        self._save_or_delete()

    def _save_or_delete(self):
        '''
        If the focused location is in the database, delete it.
        Else, save it in the database.
        '''
        if not self.focused_location:
            return

        if not self.checked:
            self._bookmark_focused_location()
            Snackbar.make('Saved in bookmarks.')
            self._show_label_dialog()
        else:
            if self._delete_bookmark(self.focused_location):
                Snackbar.make('Deleted from bookmarks.')
            else:
                Snackbar.make('Could not delete location from bookmarks.')

    def _bookmark_focused_location(self):
        location = self.focused_location
        bookmarks_facade.add_bookmark(
            label='',
            address=location['address'],
            latitude=location['latitude'],
            longitude=location['longitude']
        )
        self._check()

    def _show_label_dialog(self):
        dialog = SimpleDialogWithInput(
            title='Bookmark label',
            text='Enter label text or leave it blank:',
            on_confirm=self._on_dialog_confirm,
            on_cancel=lambda: self.dispatch('on_dialog_cancel')
        )
        dialog.bind(
            on_dismiss=lambda *x: self.dispatch('on_dialog_cancel')
        )
        dialog.open()

    def _on_dialog_confirm(self, text):
        self.dispatch('on_dialog_confirm')
        self._set_label(text)

    def _set_label(self, text):
        result = Bookmark.update(
            {'label': text},
            Query().address == self.focused_location['address']
        )

        if not result:
            return

        bookmark = bookmarks_facade.get_bookmark(
            self.focused_location['address'])
        if bookmark:
            bookmark.update_label(text.strip(' '))
            bookmark.remove_mode = False

    def _delete_bookmark(self, location):
        result = bookmarks_facade.delete_bookmark(location['address'])
        self._uncheck()
        return result
