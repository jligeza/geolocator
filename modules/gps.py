#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.event import EventDispatcher
from kivy.utils import platform
from kivy.app import App
from kivy.clock import Clock

from plyer import gps as _gps

from random import choice
from time import time


class MyGps(EventDispatcher):

    app = App.get_running_app()

    latitude = None
    longitude = None

    @property
    def pos(self):
        if self.latitude:
            return (self.latitude, self.longitude)
        return (None, None)

    _time_updated = None

    @property
    def last_update(self):
        if self._time_updated:
            return time() - self._time_updated
        else:
            return None

    def __init__(self, **kwargs):
        super(MyGps, self).__init__(**kwargs)

        _gps.configure(on_location=self._on_location)
        self.register_event_type('on_location')
        self._bind_app_events()

    def _bind_app_events(self):
        self.app.bind(on_start=self.on_app_start)
        self.app.bind(on_pause=self.on_app_pause)
        self.app.bind(on_resume=self.on_app_resume)
        self.app.bind(on_stop=self.on_app_stop)

    def on_app_start(self, *x):
        _gps.start()

    def on_app_pause(self, *x):
        _gps.stop()

    def on_app_resume(self, *x):
        _gps.start()

    def on_app_stop(self, *x):
        _gps.stop()

    def _on_location(self, **kwargs):
        self._time_updated = time()
        self.latitude = kwargs['lat']
        self.longitude = kwargs['lon']
        self.dispatch('on_location', kwargs['lat'], kwargs['lon'])

    def on_location(self, latitude, longitude):
        pass


class MockGps(EventDispatcher):

    mock_locations = ([53.44, 14.51], [53.43, 14.50], [53.44, 14.50])

    latitude = None
    longitude = None

    @property
    def pos(self):
        if self.latitude:
            return (self.latitude, self.longitude)
        return (None, None)

    _time_updated = None

    @property
    def last_update(self):
        if self._time_updated:
            return time() - self._time_updated
        else:
            return None

    def __init__(self, **kwargs):
        super(MockGps, self).__init__(**kwargs)
        self.register_event_type('on_location')
        Clock.schedule_interval(self.dispatch_random_location, 3)

    def dispatch_random_location(self, dt):
        self._time_updated = time()
        location = choice(self.mock_locations)
        self.latitude = location[0]
        self.longitude = location[1]
        self.dispatch('on_location', location[0], location[1])

    def on_location(self, latitude, longitude):
        pass

if platform == 'android':
    gps = MyGps()
else:
    print '--- real gps not available, using mock ---\n' * 3
    gps = MockGps()
