#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.event import EventDispatcher
from kivy.core.window import Window

import kivymd.snackbar as Snackbar

from time import time

ESCAPE = 27


class KeypressManager(EventDispatcher):
    '''
    An object watching globally pressed keys.

    Public methods:
        * None

    Init args:
        * app: reference to the app
        * enable_snackbar: True/False (default False)
    '''

    app = None
    enable_snackbar = True

    _time_esc_press = None

    def __init__(self, **kwargs):
        self._configure(kwargs)
        Window.bind(on_keyboard=self._special_key_filter)

    def _configure(self, kwargs):
        if 'enable_snackbar' in kwargs:
            self.enable_snackbar = kwargs['enable_snackbar']
        else:
            self.enable_snackbar = False
        self.app = kwargs['app']

    def _special_key_filter(self, window, keycode, *x):
        if keycode == ESCAPE:
            return self._handle_escape()
        return False

    def _handle_escape(self):
        if self.enable_snackbar:
            Snackbar.make('Double tap to exit.')
        if self._time_esc_press:
            time_diff = time() - self._time_esc_press
            if time_diff < 3:
                self.app.stop()
        self._time_esc_press = time()
        return True
