#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.uix.boxlayout import BoxLayout

from kivymd.label import MDLabel
from kivymd.dialog import MDDialog
from kivymd.textfields import SingleLineTextField


class SimpleDialog(MDDialog):

    def __init__(self, **kwargs):
        '''
        Keyword args:
            - title
            - text
            - on_confirm (function handle)
            - (optional) on_cancel (function handle)
        '''
        super(SimpleDialog, self).__init__(**kwargs)

        self.content_text = kwargs['text']
        self.content = self.set_content()
        self.content.bind(size=self.content.setter('size'))

        self.title = kwargs['title']

        self.size_hint = (0.8, 0.3)

        if 'on_cancel' not in kwargs.keys():
            cancel_callback = self.empty_callback
        else:
            cancel_callback = kwargs['on_cancel']

        self.set_action_buttons(kwargs['on_confirm'], cancel_callback)

    def empty_callback(self, *x):
        pass

    def set_content(self):
        '''Can be overriden for different dialog content.'''
        label = MDLabel(
            font_style='Body1',
            theme_text_color='Secondary',
            text=self.content_text,
            valign='top'
        )
        return label

    def set_action_buttons(self, confirm_callback, cancel_callback):
        self.add_action_button(
            "Confirm",
            action=lambda *x: self.on_confirm(confirm_callback)
        )
        self.add_action_button(
            "Cancel",
            action=lambda *x: self.on_cancel(cancel_callback)
        )

    def on_confirm(self, callback):
        callback()
        self.dismiss()

    def on_cancel(self, callback):
        callback()
        self.dismiss()


class SimpleDialogWithInput(SimpleDialog):
    '''
    Keyword args:
        - title
        - text
        - on_confirm (function handle)
        - (optional) on_cancel (function handle)

    Returns:
        - text: as an argument of the on_confirm callback
    '''
    text_input = None

    def set_content(self):
        label = super(SimpleDialogWithInput, self).set_content()
        self.text_input = SingleLineTextField(hint_text='Text')
        box = BoxLayout(orientation='vertical')

        box.add_widget(label)
        box.add_widget(self.text_input)

        return box

    def on_confirm(self, callback):
        callback(self.text_input.text)
        self.dismiss()
