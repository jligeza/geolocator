#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Used outside of Android, where compass is not available.
'''
from random import choice


class MockCompass(object):

    def _enable(self):
        pass

    def _disable(self):
        pass

    def _get_orientation(self):
        angles = [(350, 0, 0), (10, 0, 0)]
        return choice(angles)

    def _get_azimuth(self):
        angles = [350, 10]
        return choice(angles)

compass = MockCompass()
