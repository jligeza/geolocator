#!/usr/bin/env python
# -*- coding: utf-8 -*-
try:
    from .main import compass
except:
    print('--- real compass not available, using mock ---\n' * 3)
    from .mock import compass
from .helpers import calculate_bearing_to_target

from kivy.app import App
from kivy.uix.scatterlayout import ScatterLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.behaviors.button import ButtonBehavior
from kivy.animation import Animation
from kivy.clock import Clock, mainthread
from kivy.properties import ObjectProperty, BooleanProperty

import kivymd.snackbar as Snackbar

from time import time

# How many seconds GPS can be without updates
# till the pointer starts flickering.
GPS_ALLOWED_DESYNC = 20

NO_GPS_MESSAGE = '''
GPS has not been updated for over %s seconds.
Pointer might be inaccurate.
''' % GPS_ALLOWED_DESYNC


class Compass(FloatLayout):
    '''
    A widget with a magnetic disk showing the azimuth, and a pointer
    which can be used to show the shortest path to a destination
    (required GPS coords).

    Compass is enabled automatically upon being added to a widget tree.

    Public methods:
        * update_bearing_to_target(my_pos, destination)
    '''

    app = App.get_running_app()

    bearing_to_target = 0
    disk = ObjectProperty()
    pointer = ObjectProperty()

    _last_gps_update = None
    arrow_hidden = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(Compass, self).__init__(**kwargs)
        self._bind_events()
        self._configure_pointer()
        Clock.schedule_once(lambda dt: self.disk.start())
        Clock.schedule_interval(self._check_gps_freshness, 1)

    @mainthread
    def _bind_events(self):
        self.app.bind(on_pause=lambda *x: self.disk.stop())
        self.app.bind(on_resume=lambda *x: self.disk.start())
        self.app.bind(on_stop=lambda *x: self.disk.stop())

    @mainthread
    def _configure_pointer(self):
        self.disk.bind(on_disk_rotation=self.on_disk_rotation)

    def _check_gps_freshness(self, *x):
        if self._last_gps_update is None:
            return

        if time() - self._last_gps_update > GPS_ALLOWED_DESYNC:
            self.pointer.start_pulse()
        else:
            self.pointer.stop_pulse()

    def on_disk_rotation(self, obj, angle):
        bearing = -1 * self.bearing_to_target + angle
        self.pointer.rotation = bearing

    def update_bearing_to_target(self, my_pos, destination):
        self.bearing_to_target = calculate_bearing_to_target(
            my_pos, destination
        )
        self._last_gps_update = time()

    def on_arrow_hidden(self, obj, state):
        if state is True:
            self.pointer.hide()
        else:
            self.pointer.show()


class Disk(ScatterLayout):
    '''
    A magnetic disk widget, displaying the orientation of the cardinal
    directions.

    Public methods:
        * start()
        * stop()
    '''

    COMPASS_REFRESH_RATE = 0.3

    def __init__(self, **kwargs):
        super(Disk, self).__init__(**kwargs)
        self.register_event_type('on_disk_rotation')

    def on_disk_rotation(self, angle):
        pass

    def on_rotation(self, obj, angle):
        self.dispatch('on_disk_rotation', angle)

    def start(self):
        compass._enable()
        Clock.schedule_interval(
            self._refresh,
            self.COMPASS_REFRESH_RATE
        )

    def stop(self):
        Clock.unschedule(self._refresh)
        compass._disable()

    def get_azimuth(self):
        azimuth = compass._get_azimuth()
        return azimuth if azimuth else 0

    def _refresh(self, *x):
        azimuth = self.get_azimuth()

        azimuth_diff = self.rotation - azimuth
        sign = -1 if self.rotation - azimuth < 0 else 1

        if self._overturn(azimuth_diff):
            azimuth_diff = self.rotation - (360 * sign) - azimuth

        self._turn_compass(azimuth_diff)

    def _overturn(self, angle_diff):
        return True if abs(angle_diff) > 180 else False

    def _turn_compass(self, angle):
        anim = Animation(
            rotation=self.rotation - angle,
            transition='linear',
            duration=self.COMPASS_REFRESH_RATE - 0.05
        )
        anim.start(self)


class Pointer(ScatterLayout):
    '''
    A red arrow widget, pointing toward a demanded azimuth.

    Public methods:
        * hide()
        * show()
        * start_pulse()
        * stop_pulse()
    '''

    _hidden = BooleanProperty(False)
    _is_flickering = False

    def hide(self):
        '''Hide arrow from view.'''
        self._hidden = True
        self.opacity = 0

    def show(self):
        '''Show arrow.'''
        self._hidden = False
        self.opacity = 1

    def start_pulse(self):
        '''Make opacity flicker.'''
        if self._hidden is False:
            Clock.schedule_interval(self._blur_animation, 1)
            self._is_flickering = True

    def _blur_animation(self, dt):

        def emerge(*x):
            emerge = Animation(opacity=1, duration=0.5)
            emerge.start(self)

        fade = Animation(opacity=0.2, duration=0.5)
        fade.bind(on_complete=emerge)
        fade.start(self)

    def stop_pulse(self):
        '''Stop opacity flickering.'''
        Clock.unschedule(self._blur_animation)
        self._is_flickering = False

    def on__hidden(self, obj, state):
        if state is True:
            self.stop_pulse()

    def on_pointer_press(self):
        if self._is_flickering:
            Snackbar.make(NO_GPS_MESSAGE)


class ClickableImage(ButtonBehavior, Image):
    pass
