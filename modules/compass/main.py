#!/usr/bin/env python
# -*- coding: utf-8 -*-
from jnius import PythonJavaClass, java_method, autoclass, cast
from plyer.platforms.android import activity

from math import degrees

Context = autoclass('android.content.Context')
Sensor = autoclass('android.hardware.Sensor')
SensorManager = autoclass('android.hardware.SensorManager')


class RotationVectorListener(PythonJavaClass):
    __javainterfaces__ = ['android/hardware/SensorEventListener']

    def __init__(self):
        super(RotationVectorListener, self).__init__()
        self.SensorManager = cast(
            'android.hardware.SensorManager',
            activity.getSystemService(Context.SENSOR_SERVICE)
        )
        self.sensor = self.SensorManager.getDefaultSensor(
            Sensor.TYPE_ROTATION_VECTOR)

        self.orientation = [0.0] * 3
        self.values = [None] * 3
        self.rotation_matrix = [0.0] * 9
        self.azimuth = None

    def enable(self):
        self.SensorManager.registerListener(
            self,
            self.sensor,
            SensorManager.SENSOR_DELAY_NORMAL
        )

    def disable(self):
        self.SensorManager.unregisterListener(self, self.sensor)

    @java_method('(Landroid/hardware/SensorEvent;)V')
    def onSensorChanged(self, event):
        self.values = event.values[:3]
        self.SensorManager.getRotationMatrixFromVector(
            self.rotation_matrix, self.values
        )
        self.azimuth =\
            int(
                degrees(
                    self.SensorManager.getOrientation(
                        self.rotation_matrix, self.orientation
                    )[0]
                ) + 360
            ) % 360

    @java_method('(Landroid/hardware/Sensor;I)V')
    def onAccuracyChanged(self, sensor, accuracy):
        # Maybe, do something in future?
        pass


class AndroidCompass(object):

    def __init__(self):
        self.bState = False

    def _enable(self):
        if (not self.bState):
            self.listener = RotationVectorListener()
            self.listener.enable()
            self.bState = True

    def _disable(self):
        if (self.bState):
            self.bState = False
            self.listener.disable()
            del self.listener

    def _get_orientation(self):
        if (self.bState):
            return tuple(self.listener.orientation)
        else:
            return (None, None, None)

    def _get_azimuth(self):
        if (self.bState):
            return self.listener.azimuth
        else:
            return None

    def __del__(self):
        if(self.bState):
            self._disable()
        super(self.__class__, self).__del__()


compass = AndroidCompass()
