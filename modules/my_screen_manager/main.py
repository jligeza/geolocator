#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.uix.screenmanager import ScreenManager
from kivy.properties import ObjectProperty
from kivy.clock import mainthread


class MyScreenManager(ScreenManager):

    main_screen = ObjectProperty()
    history_screen = ObjectProperty()
    bookmarks_screen = ObjectProperty()

    def __init__(self, **kwargs):
        super(MyScreenManager, self).__init__(**kwargs)
        self._configure()

    def on_screen_swap(self, screen_name):
        pass

    @mainthread
    def _configure(self):
        self.main_screen.configure(self._save_in_history)
        self.history_screen.configure(self._quick_search)
        self.bookmarks_screen.configure(self._quick_search)

    @mainthread
    def _save_in_history(self, location):
        self.history_screen.add_record(
            address=location.address,
            latitude=location.latitude,
            longitude=location.longitude
        )

    def _quick_search(self, record):
        self.current = 'main'
        self.main_screen.quick_search(record)
