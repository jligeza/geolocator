#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.uix.screenmanager import Screen
from kivy.properties import ObjectProperty, BooleanProperty
from kivy.clock import Clock, mainthread

from geopy.geocoders import GoogleV3
from geopy.exc import GeopyError

from threading import Thread
from functools import wraps

from ..gps import gps


class MyLocationScreen(Screen):
    '''
    A screen on which a user can display their current location.

    Public methods:
        * async_refresh
    '''

    bookmark_button = ObjectProperty()
    address_label = ObjectProperty()
    gps_label = ObjectProperty()
    spinner = ObjectProperty()

    thread_is_running = BooleanProperty(False)

    google = GoogleV3()

    def on_enter(self):
        self.async_refresh()
        Clock.schedule_interval(self._measure_gps_freshness, 1)
        self.start_pos_refresh()

        self.bookmark_button.bind(on_press=self.on_bookmark_button_press)
        self.bookmark_button.bind(on_dialog_confirm=self.start_pos_refresh)
        self.bookmark_button.bind(on_dialog_cancel=self.start_pos_refresh)

    def on_bookmark_button_press(self, obj):
        if obj.checked is True:
            self.start_pos_refresh()
        else:
            self.stop_pos_refresh()

    def async_refresh(self):
        '''
        Refresh location without blocking gui.

        Uses current coordinates supported by a gps.
        '''
        if self.thread_is_running is True:
            return

        if not gps.latitude:
            return self._handle_error(reason='gps')

        self._toggle_thread_flag(True)

        thread = Thread(
            target=self._refresh,
            args=(gps.pos, self._focus_location)
        )
        thread.daemon = True
        thread.start()

    def _focus_location(self, location):
        self.set_address_label(location.address)
        self.bookmark_button.focus_location(location, custom_pos=gps.pos)

    def spinner_progress(function):
        @wraps(function)
        def wrapped(self, *args):
            self._toggle_spinner()
            function(self, *args)
            self._toggle_spinner()
        return wrapped

    @mainthread
    def _toggle_spinner(self):
        self.spinner.active = not self.spinner.active

    @mainthread
    def _toggle_thread_flag(self, state):
        self.thread_is_running = state

    @spinner_progress
    def _refresh(self, pos, callback):
        '''Used only by a thread.'''
        result = self._query_google(pos)

        if result['success']:
            @mainthread
            def job():
                callback(result['location'])
            job()
        else:
            self._handle_error(result['reason'])

        self._toggle_thread_flag(False)

    def _query_google(self, pos):
        '''
        Query google and retrieve results.

        Arguments:
            * pos: (latitude, longitude)

        Returns:
            {'success': True, 'location': location}
            {'success': False, 'reason': 'no_net' or 'not_found'}
        '''
        string = '{}, {}'.format(pos[0], pos[1])
        try:
            location = self.google.reverse(string, exactly_one=True)
        except GeopyError:
            return {'success': False, 'reason': 'no_net'}

        if not location or not location.latitude:
            return {'success': False, 'reason': 'not_found'}
        else:
            return {'success': True, 'location': location}

    @mainthread
    def _handle_error(self, reason):
        show = self.set_address_label
        if reason is 'no_net':
            show('No internet connection')
        elif reason is 'not_found':
            show('Location not found')
        elif reason is 'gps':
            show('Could not receive GPS coords')

    def set_address_label(self, address):
        self.address_label.text = address

    def _measure_gps_freshness(self, *x):
        time = gps.last_update
        if not time:
            return
        time = int(time)
        self.gps_label.text = 'last GPS update: %s seconds ago' % time

    def on_gps_location(self, *x):
        self.async_refresh()

    def stop_pos_refresh(self, *x):
        gps.unbind(on_location=self.on_gps_location)

    def start_pos_refresh(self, *x):
        gps.bind(on_location=self.on_gps_location)

    def on_leave(self):
        Clock.unschedule(self._measure_gps_freshness)
        self.stop_pos_refresh()
