__all__ = ['MyLocationScreen']
from .screen import MyLocationScreen


def load_gui():
    from kivy.lang import Builder
    import os
    file_path = os.path.dirname(os.path.realpath(__file__))
    Builder.load_file(file_path + '/gui.kv')

load_gui()
