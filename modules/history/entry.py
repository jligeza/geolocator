#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.uix.gridlayout import GridLayout
from kivy.properties import StringProperty, BooleanProperty


class HistoryEntry(GridLayout):

    latitude = None
    longitude = None
    address = StringProperty()
    id = None

    remove_mode = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(HistoryEntry, self).__init__(**kwargs)
        self.latitude = kwargs['latitude']
        self.longitude = kwargs['longitude']
        self.address = kwargs['address']
        self.id = kwargs['id']

        self.register_event_type('on_press')

    def on_press(self, *x):
        pass

    def toggle_remove_mode(self):
        self.remove_mode = not self.remove_mode

    def search_me(self):
        self.dispatch('on_press')
