#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.properties import ObjectProperty
from kivy.clock import Clock

from kivymd.list import MDList

from .service import service
from .entry import HistoryEntry


class HistoryList(MDList):

    history_screen = ObjectProperty()
    _records_already_loaded = False

    def load_records(self):
        '''
        Load records from database, if they weren't loaded yet.

        Delayed to prevent user being stuck at navbar when
        records are being loaded.
        '''
        if self._records_already_loaded is False:
            def delayed_job(dt):
                self._show_database_entries_on_screen()
            self._records_already_loaded = True
            Clock.schedule_once(delayed_job, 0.3)

    def _show_database_entries_on_screen(self):
        '''
        Query database and put results on scroll view.
        '''
        for record in service.fetch_all():
            entry = HistoryEntry(
                address=record['address'],
                latitude=record['latitude'],
                longitude=record['longitude'],
                id=record.eid
            )
            self.add_widget(entry)

    def add_widget(self, widget, index=0):
        '''
        Override - appends elements at the top of list,
        and binds `on_press` events.
        '''
        super(HistoryList, self).add_widget(widget, len(self.children))
        widget.bind(on_press=self._search_record)

    def _search_record(self, record):
        '''Used when pressed a history entry.'''
        self.history_screen.search_record(record)

    def add_record(self, address, latitude, longitude):
        '''
        Add new history entry.
        Save it in database, and display on a scroll view.
        '''
        record = HistoryEntry(
            address=address,
            latitude=latitude,
            longitude=longitude,
            id=None
        )

        result = service.save(record)
        record.id = result

        if result and self._records_already_loaded is True:
            self.add_widget(record)

    def delete_marked_records(self):
        '''
        Delete records marked for removal.
        '''
        to_delete = []
        for record in self.children:
            if record.remove_mode:
                to_delete.append(record)

        if to_delete:
            self.clear_widgets(to_delete)

        service.delete(*to_delete)

    def mark_all_records_for_removal(self):
        for record in self.children:
            record.remove_mode = True
