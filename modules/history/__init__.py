__all__ = ['HistoryScreen', 'HistoryList', 'HistoryEntry']
from .screen import HistoryScreen
from .list import HistoryList
from .entry import HistoryEntry


def load_gui():
    from kivy.lang import Builder
    import os
    file_path = os.path.dirname(os.path.realpath(__file__))
    Builder.load_file(file_path + '/gui.kv')

load_gui()
