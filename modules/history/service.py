#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..database import History

from tinydb import Query


class HistoryService(object):

    def fetch_all(self):
        '''
        Return all records from table.
        '''
        return History.all()

    def save(self, obj):
        '''
        Save history object in table, if already not present.

        Returns:
            - record eid, if saved in database,
            - None, if already in database.
        '''
        if self._already_in_table(obj.address):
            return

        eid = History.insert({
            'address': obj.address,
            'latitude': obj.latitude,
            'longitude': obj.longitude
        })
        return eid

    def _already_in_table(self, address):
        if History.get(Query().address == address):
            return True
        else:
            return False

    def delete(self, *entries):
        '''
        Delete given entries.
        '''
        if len(entries) == 1:
            History.remove(eids=(entries[0].id,))
        else:
            eids = [entry.id for entry in entries]
            History.remove(eids=eids)


service = HistoryService()
