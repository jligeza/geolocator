#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.uix.screenmanager import Screen
from kivy.properties import ObjectProperty
from kivy.app import App

from ..utils import SimpleDialog


class HistoryScreen(Screen):
    '''
    Displays previously searched addresses.

    Requires configuration.

    Public methods:
        * configure(callback)
        * search_record()
        * add_record()
    '''

    history_list = ObjectProperty()
    app = App.get_running_app()

    _searching_function = None
    _configured = False

    def __init__(self, **kwargs):
        super(HistoryScreen, self).__init__(**kwargs)
        self.app.bind(on_stop=lambda *x: self.on_leave())

    def on_leave(self):
        self.history_list.delete_marked_records()

    def configure(self, callback):
        '''Set callback to searching function.'''
        self._searching_function = callback
        self._configured = True

    def on_enter(self):
        if not self._configured:
            raise Exception('screen not configured')
        self.history_list.load_records()

    def _show_removal_dialog(self):
        '''Mark all records for deletion if confirmed.'''
        dialog = SimpleDialog(
            title='Clear history',
            text='Would you like to remove all history records?',
            on_confirm=self._remove_all_history_records
        )
        dialog.open()

    def _remove_all_history_records(self):
        self.history_list.mark_all_records_for_removal()

    def search_record(self, record):
        self._searching_function(record)

    def add_record(self, address, latitude, longitude):
        self.history_list.add_record(address, latitude, longitude)
