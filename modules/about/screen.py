#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.uix.screenmanager import Screen

from plyer import email


class AboutScreen(Screen):

    def send_email(self):
        email.send(recipient='jakub.jaroslaw.ligeza@gmail.com')
