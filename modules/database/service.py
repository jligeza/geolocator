#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .helpers import(
    set_cross_platform_database_path,
    clean_old_peewee_db
)

from tinydb import TinyDB

db_path = set_cross_platform_database_path('geolocator.json')

db = TinyDB(db_path)

History = db.table('history')
Bookmark = db.table('bookmarks')

clean_old_peewee_db()
