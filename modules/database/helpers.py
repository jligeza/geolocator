#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.utils import platform
from kivy.clock import Clock

import kivymd.snackbar as Snackbar

import os


def set_cross_platform_database_path(db_name):
    '''
    On Linux, return the name unchanged.

    On Android device, prepend name with SD card path to ensure
    data presistence between shop updates.

    If there was an error accessing the SD card, displays the error
    on a snackbar.
    '''
    if platform == 'android':
        return _android_db_path(db_name)
    else:
        return db_name


def _android_db_path(db_name):
    result = _prepend_android_sdcard_path(db_name)

    if result['success'] is False:
        error = 'Could not access SD card.'
        error += ' Data will not be persistent between shop updates.'
        Clock.schedule_once(
            lambda dt: Snackbar.make(error, duration=10), 3
        )
        return db_name
    else:
        return result['db_path']


def _prepend_android_sdcard_path(db_path):
    from jnius import autoclass
    Environment = autoclass('android.os.Environment')
    sdpath = Environment.getExternalStorageDirectory().getAbsolutePath()
    is_mounted = Environment.getExternalStorageState() ==\
        Environment.MEDIA_MOUNTED

    if not sdpath or not is_mounted:
        return {'success': False, 'db_path': db_path}
    else:
        db_path = '{}/{}'.format(sdpath, db_path)
        return {'success': True, 'db_path': db_path}


def clean_old_peewee_db():
    db_path = set_cross_platform_database_path('geolocator.db')
    try:
        os.remove(db_path)
    except:
        pass
