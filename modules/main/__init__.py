__all__ = ['MainScreen', 'BookmarkButton', 'Compass']
from .screen import MainScreen

from ..bookmark_button import BookmarkButton
from ..compass import Compass


def load_gui():
    from kivy.lang import Builder
    import os
    file_path = os.path.dirname(os.path.realpath(__file__))
    Builder.load_file(file_path + '/gui.kv')

load_gui()
