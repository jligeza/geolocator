#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.properties import (
    ObjectProperty,
    BooleanProperty,
    NumericProperty,
    ReferenceListProperty
)
from kivy.uix.screenmanager import Screen
from kivy.clock import mainthread

from kivymd.label import MDLabel

from ..gps import gps
from . import messages as _

from threading import Thread

from geopy.geocoders import GoogleV3
from geopy.distance import vincenty


class MainScreen(Screen):
    '''
    Location seach screen.

    Requires configuration.

    Public methods:
        * configure(callback)
        * search(address)
        * quick_search(entry)
    '''

    address_input = ObjectProperty()
    target_name_label = ObjectProperty()
    target_distance_label = ObjectProperty()
    compass = ObjectProperty()
    bookmark_button = ObjectProperty()

    num_location_error = 0
    loading_address = BooleanProperty(False)
    gps_error = False

    found_location = ObjectProperty(allownone=True)

    google = GoogleV3()

    latitude = NumericProperty(None)
    longitude = NumericProperty(None)
    my_pos = ReferenceListProperty(latitude, longitude)

    target_latitude = NumericProperty(None)
    target_longitude = NumericProperty(None)
    target_pos = ReferenceListProperty(target_latitude, target_longitude)
    bearing_to_target = NumericProperty(0)

    _history_saving_function = None
    _configured = False

    def configure(self, history_save_callback):
        '''Set history save callback. Must be thread safe.'''
        self._history_saving_function = history_save_callback
        self._configured = True

    def __init__(self, **kwargs):
        super(MainScreen, self).__init__(**kwargs)
        self._bind_events()

    @mainthread
    def _bind_events(self):
        gps.bind(on_location=self.on_location)
        self.address_input.bind(
            on_text_validate=self._on_address_enter
        )

    def on_location(self, obj, latitude, longitude):
        self._set_my_pos(latitude, longitude)

        if self.found_location:
            self._update_target_distance_label()
            self._set_pointer_at_target()

    def _set_my_pos(self, latitude, longitude):
        self.my_pos = (latitude, longitude)

    def _on_address_enter(self, obj):
        address = obj.text
        if address:
            self.search(address)

    def search(self, address):
        '''
        Search for given address using google.
        Displays location on the screen's labels.
        Needs GPS and internet connection.
        '''
        if not self._configured:
            raise Exception('screen not configured')

        def job():
            location = self._query_google(address)
            thread_safe_part(location)

        @mainthread
        def thread_safe_part(location):
            self.loading_address = False
            self.search_method = 'normal'
            self.found_location = None

            if not self.received_coords():
                return self.display_gps_error()

            if location == -1:
                return self._display_internet_error()

            if not location:
                return self._display_location_error()

            self.found_location = location

            self.target_pos = (location.latitude, location.longitude)
            self._show_location_on_screen(location)
            self._history_saving_function(location)
            self._set_pointer_at_target()

        if self.loading_address is False:
            self.loading_address = True
            thread = Thread(target=job)
            thread.daemon = True
            thread.start()

    def _query_google(self, address):
        '''
        Query google and retrieve results.

        Returns:
            * on success: location
            * on internet error: -1
            * on location not found: False
        '''
        try:
            location = self.google.geocode(address)
        except:
            return -1

        if not location or not location.latitude:
            return False
        else:
            return location

    def received_coords(self):
        return self.my_pos[0] is not None and self.my_pos[1] is not None

    def display_gps_error(self):
        self.target_name_label.text = _.GPS_ERROR_TOP
        self.target_distance_label.text = _.GPS_ERROR_BOTTOM
        self.gps_error = True

    def _display_internet_error(self):
        self.target_name_label.text = _.INTERNET_ERROR_TOP
        self.target_distance_label.text = _.INTERNET_ERROR_BOTTOM

    def on_found_location(self, obj, location):
        if location:
            self.bookmark_button.focus_location(location)
            self.num_location_error = 0

    def on_my_pos(self, latitude, longitude):
        if self.gps_error:
            self.gps_error = False
            if self.search_method == 'normal':
                self.search(self.address_input.text)
            elif self.search_method == 'quick':
                self.quick_search(self.found_location)

    def quick_search(self, record):
        '''
        Search offline from history or bookmarks.
        '''
        self.search_method = 'quick'

        self.found_location = record

        if not self.received_coords():
            return self.display_gps_error()

        self.target_pos = (record.latitude, record.longitude)

        self.address_input.text = ''
        self._show_location_on_screen(record)
        self._set_pointer_at_target()
        return True

    def _update_target_distance_label(self):
        if not self.target_pos[0]:
            return

        self.target_distance_label.update_distance(
            self.my_pos, self.target_pos
        )

    def _display_location_error(self):
        self.num_location_error += 1
        self.target_name_label.text = '{}. {}'.format(
            (self.num_location_error + 1),
            _.LOCATION_ERROR_TOP
        )
        self.target_distance_label.text = _.LOCAION_ERROR_BOTTOM

    def _show_location_on_screen(self, location):
        self.target_name_label.text = location.address
        self._update_target_distance_label()

    @mainthread
    def _set_pointer_at_target(self):
        if not self.target_pos[0]:
            return

        self.compass.update_bearing_to_target(
            (self.latitude, self.longitude),
            (self.target_latitude, self.target_longitude)
        )


class DistanceLabel(MDLabel):

    def update_distance(self, point_A, point_B):
        '''
        Display pretty distance between two points.
        '''
        points = (point_A, point_B)
        pretty_dist = self._prettify_distance(vincenty(*points).m)

        string = 'Distance: '
        string += pretty_dist['km'] + ' km'
        string += ', ' + pretty_dist['rest_m'] + ' m'

        self.text = string

    def _prettify_distance(self, meters):
        km = int(meters / 1000)
        km_ = km * 1000
        rest_m = int(meters - km_)
        return {'km': str(km), 'rest_m': str(rest_m)}
