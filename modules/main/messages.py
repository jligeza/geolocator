#!/usr/bin/env python
# -*- coding: utf-8 -*-

GPS_ERROR_TOP = 'Could not receive GPS coords'
GPS_ERROR_BOTTOM = 'Search will reload automatically once GPS is available.'

INTERNET_ERROR_TOP = 'No internet connection'
INTERNET_ERROR_BOTTOM = 'Without it, you can only use locations from history or bookmarks.'

LOCATION_ERROR_TOP = 'Location not found'
LOCAION_ERROR_BOTTOM = ''
