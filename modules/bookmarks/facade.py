#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.app import App
from kivy.properties import ObjectProperty
from kivy.clock import mainthread
from kivy.event import EventDispatcher

from .service import service


class Facade(EventDispatcher):
    '''
    Simplifies interaction of outside widgets with bookmarks screen.

    Public methods:
        * add_bookmark(**kwargs)
        * delete_bookmark(address)
        * already_in_table(address)
        * get_bookmark(address)

    Events:
        * on_bookmarks_deleted(bookmarks)
    '''

    bookmarks_list = ObjectProperty()

    def configure(self, bookmarks_list):
        bookmarks_list.bind(
            on_bookmarks_deleted=self._on_bookmarks_deleted)

    def __init__(self, **kwargs):
        super(Facade, self).__init__(**kwargs)
        self._bind_properties()
        self.register_event_type('on_bookmarks_deleted')

    @mainthread
    def _bind_properties(self):
        app = App.get_running_app()
        self.bookmarks_list = app.get('bookmarks_screen.bookmarks_list')
        self.bookmarks_list.bind(
            on_bookmarks_deleted=self._on_bookmarks_deleted)

    def _on_bookmarks_deleted(self, obj, bookmarks):
        self.dispatch('on_bookmarks_deleted', bookmarks)

    def on_bookmarks_deleted(self, bookmarks):
        pass

    def add_bookmark(self, **kwargs):
        '''
        Add new bookmark entry.
        Save it in database, and display on a scroll view.

        Returns:
            * {'record': database_entry, 'already_in_table': bool}
        '''
        return self.bookmarks_list.add_record(**kwargs)

    def delete_bookmark(self, address):
        if self.bookmarks_list._records_already_loaded:
            to_delete = None
            for child in self.bookmarks_list.children:
                if child.address == address:
                    to_delete = child
                    break

            if to_delete:
                to_delete.remove_mode = True

            return True if to_delete else False
        else:
            return service.delete_by_address(address)

    def already_in_table(self, address):
        return service.already_in_table(address)

    def get_bookmark(self, address):
        '''
        Get bookmark object from bookmark list.

        Returns:
            * entry - on success
            * None - no child with such address
            * False - no children on list
        '''
        if len(self.bookmarks_list.children) > 0:
            for child in self.bookmarks_list.children:
                if child.address == address:
                    return child
            return None
        else:
            return False

facade = Facade()
