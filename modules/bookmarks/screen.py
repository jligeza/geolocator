#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.uix.screenmanager import Screen
from kivy.app import App

from ..utils import SimpleDialog


class BookmarksScreen(Screen):
    '''
    Displays bookmarked locations.

    Requires configuration.

    Public methods:
        * add_record(record) return False/True
        * search_record(record)
        * show_removal_dialog()
    '''

    bookmarks_list = None
    app = App.get_running_app()

    _searching_function = None
    _configured = False

    def __init__(self, **kwargs):
        super(BookmarksScreen, self).__init__(**kwargs)
        self.app.bind(on_stop=lambda *x: self.on_leave())

    def on_leave(self):
        self.bookmarks_list.delete_marked_records()

    def configure(self, callback):
        '''Set callback to searching function.'''
        self._searching_function = callback
        self._configured = True

    def on_enter(self):
        if not self._configured:
            raise Exception('screen not configured')
        self.bookmarks_list.load_records()

    def show_removal_dialog(self):
        '''Mark all records for deletion if confirmed.'''
        dialog = SimpleDialog(
            title='Clear bookmarks',
            text='Would you like to remove all bookmarks?',
            on_confirm=self._remove_all_bookmarks
        )
        dialog.open()

    def _remove_all_bookmarks(self):
        self.bookmarks_list.mark_all_records_for_removal()

    def search_record(self, record):
        self._searching_function(record)

    def add_record(self, **record):
        return self.bookmarks_list.add_record(**record)
