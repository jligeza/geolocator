#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.clock import Clock, mainthread
from kivy.properties import ObjectProperty

from kivymd.list import MDList

from .service import service
from .entry import BookmarkEntry


class BookmarksList(MDList):
    '''
    Displays bookmarked entries on a grid.

    Public methods:
        * load_records()
        * add_record(record) return False/True
    '''

    bookmarks_screen = ObjectProperty()
    _records_already_loaded = False

    def __init__(self, **kwargs):
        super(BookmarksList, self).__init__(**kwargs)
        self.register_event_type('on_bookmarks_deleted')

    def on_bookmarks_deleted(self, bookmarks):
        '''Dispatched when some bookmarks have been deleted.'''
        pass

    def load_records(self):
        '''
        Load records from database, if they weren't loaded yet.

        Delayed to prevent user being stuck at navbar when
        records are being loaded.
        '''
        if self._records_already_loaded is False:
            def delayed_job(dt):
                self._show_database_entries_on_screen()
            self._records_already_loaded = True
            Clock.schedule_once(delayed_job, 0.3)

    def _show_database_entries_on_screen(self):
        '''
        Query database and put results on scroll view.
        '''
        for record in service.fetch_all():
            entry = BookmarkEntry(
                label=record['label'],
                address=record['address'],
                latitude=record['latitude'],
                longitude=record['longitude'],
                id=record.eid
            )
            self.add_widget(entry)

    def add_widget(self, widget, index=0):
        '''
        Override - appends elements at the top of list,
        and binds `on_press` events.
        '''
        super(BookmarksList, self).add_widget(widget, len(self.children))
        widget.bind(on_press=self._search_record)

    def _search_record(self, record):
        '''Used when pressed a bookmark entry.'''
        self.bookmarks_screen.search_record(record)

    def add_record(self, label, address, latitude, longitude):
        '''
        Add new bookmark entry.
        Save it in database, and display on a scroll view.

        Returns:
            * {'record': database_entry, 'already_in_table': bool}
        '''
        record = BookmarkEntry(
            label=label,
            address=address,
            latitude=latitude,
            longitude=longitude,
            id=None
        )

        result = service.save(record)
        record.id = result['eid']

        if not result['already_in_table'] and\
                self._records_already_loaded is True:
            self.add_widget(record)

        return {'record': record, 'already_in_table': result}

    def delete_marked_records(self):
        '''
        Delete records marked for removal.
        '''
        to_delete = []
        for record in self.children:
            if record.remove_mode:
                to_delete.append(record)
        service.delete(*to_delete)

        if to_delete:
            self.clear_widgets(to_delete)

        self._broadcast_deleted_bookmarks(to_delete)

    @mainthread
    def _broadcast_deleted_bookmarks(self, bookmarks):
        '''Used by bookmark button to to uncheck itself.'''
        self.dispatch('on_bookmarks_deleted', bookmarks)

    def mark_all_records_for_removal(self):
        for record in self.children:
            record.remove_mode = True
