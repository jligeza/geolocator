#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..database import Bookmark

from tinydb import Query


class BookmarkService(object):

    def fetch_all(self):
        '''
        Return all records from table.
        '''
        return Bookmark.all()

    def save(self, obj):
        '''
        Save bookmark object in table, if already not present.

        Returns:
            {'eid': eid, 'already_in_table': bool}
        '''
        eid = Bookmark.get(Query().address == obj.address)
        if eid:
            return {'eid': eid, 'already_in_table': True}

        eid = Bookmark.insert({
            'label': obj.label,
            'address': obj.address,
            'latitude': obj.latitude,
            'longitude': obj.longitude
        })
        return {'eid': eid, 'already_in_table': False}

    def already_in_table(self, address):
        if Bookmark.get(Query().address == address):
            return True
        else:
            return False

    def delete_by_address(self, address):
        '''
        Delete an entry from the database, by address.
        '''
        if Bookmark.remove(Query().address == address):
            return True
        else:
            return False

    def delete(self, *entries):
        '''
        Delete given entries.
        '''
        if len(entries) == 1:
            Bookmark.remove(eids=(entries[0].id,))
        else:
            eids = [entry.id for entry in entries]
            Bookmark.remove(eids=eids)


service = BookmarkService()
