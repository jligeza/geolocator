__all__ = ['BookmarksScreen', 'BookmarksList', 'BookmarkEntry', 'facade']
from .screen import BookmarksScreen
from .list import BookmarksList
from .entry import BookmarkEntry
from .facade import facade


def load_gui():
    from kivy.lang import Builder
    import os
    file_path = os.path.dirname(os.path.realpath(__file__))
    Builder.load_file(file_path + '/gui.kv')

load_gui()
