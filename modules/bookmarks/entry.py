#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.uix.gridlayout import GridLayout
from kivy.properties import StringProperty, BooleanProperty

from kivymd.menu import MDDropdownMenu

from ..utils import SimpleDialogWithInput
from ..database import Bookmark


class BookmarkEntry(GridLayout):

    _text = StringProperty()
    label = None
    latitude = None
    longitude = None
    address = None
    id = None

    remove_mode = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(BookmarkEntry, self).__init__(**kwargs)
        self.label = kwargs['label']
        self.latitude = kwargs['latitude']
        self.longitude = kwargs['longitude']
        self.address = kwargs['address']
        self.id = kwargs['id']

        if self.label:
            self._text = self.label
        else:
            self._text = self.address

        self.register_event_type('on_press')

    def on_press(self, *x):
        pass

    def _handle_icon_press(self):
        if self.remove_mode:
            self.remove_mode = False
        else:
            self._show_dropdown_menu()

    def update_label(self, new_label):
        if new_label:
            self._text = new_label
        else:
            self._text = self.address

        Bookmark.update({'label': new_label}, eids=[self.id, ])

    def search_me(self):
        self.dispatch('on_press')

    def on__text(self, obj, value):
        if len(value) > 70:
            self.ids.label_text = value[:70]

    def _show_dropdown_menu(self):
        DropdownMenu(record=self, width_mult=3).open(self)


class DropdownMenu(MDDropdownMenu):

    record = None

    def __init__(self, **kwargs):
        super(DropdownMenu, self).__init__(**kwargs)

        self.record = kwargs['record']

        self.items = [
            {
                'viewclass': 'MDMenuItem',
                'text': 'Edit label',
                'on_release': self.edit
            },
            {
                'viewclass': 'MDMenuItem',
                'text': 'Clear label',
                'on_release': self.clear
            },
            {
                'viewclass': 'MDMenuItem',
                'text': 'Remove',
                'on_release': self.remove
            }
        ]

    def edit(self):
        dialog = SimpleDialogWithInput(
            title='Edit label',
            text='Enter new label text:',
            on_confirm=self._set_label,
        )
        dialog.open()

    def _set_label(self, text):
        stripped = text.strip(' ')
        if stripped:
            self.record.update_label(stripped)
        self.dismiss()

    def clear(self):
        self.record.update_label('')
        self.dismiss()

    def remove(self):
        self.record.remove_mode = True
        self.dismiss()
