[app]

title = Simple GeoLocator
package.name = geolocator
package.domain = org.jligeza 
source.dir = .  
source.include_exts = py,png,jpg,kv,gif,rst,ttf,atlas
source.exclude_dirs = bin,shop_images,python-for-android-old_toolchain
version = 0.1.3
requirements = openssl,geopy,plyer,tinydb,kivy==master
# kivyMD link: git+https://gitlab.com/kivymd/KivyMD.git
garden_requirements = recycleview
orientation = portrait
fullscreen = 1 
android.api = 19 
android.minapi = 13 
presplash.filename = images/logo.png
icon.filename = images/logo.png
android.permissions = INTERNET, ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION

android.p4a_whitelist = lib-dynload/_csv.so
android.p4a_dir = python-for-android-old_toolchain


[buildozer] 

log_level = 2
warn_on_root = 1
