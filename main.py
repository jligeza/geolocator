#!/usr/bin/env python2
# -*- coding: utf-8 -*-
from kivy.app import App

from kivymd.theming import ThemeManager

from modules.keypress_manager import KeypressManager


class GeoLocator(App):
    theme_cls = ThemeManager()

    def get(self, widget_name):
        '''Return widget by its name, using tree of properties.'''
        return eval('self.root.screen_manager.%s' % widget_name)

    def on_pause(self):
        return True

    def open_settings(*args):
        pass

    def build(self):
        self.theme_cls.theme_style = 'Light'
        self.theme_cls.primary_palette = 'Indigo'
        self.key_press_manager = KeypressManager(
            app=self,
            enable_snackbar=True
        )


GeoLocator().run()
