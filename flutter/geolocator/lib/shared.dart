import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:google_maps_webservice/geocoding.dart' as gc;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart' as p;
import './store.dart' show StoreProvider;
import './shared.dart' as sh;
import 'package:rxdart/rxdart.dart' as rx;

enum LocationAction { remove, details }

class Locations extends StatefulWidget {
  final rx.BehaviorSubject<List<sh.Location>> stream;
  final String emptyTxt;
  final void Function() onLocationTap;
  final Widget Function(sh.Location) locationIcon;
  final void Function() onDeleteAll;
  final void Function(sh.Location) onDelete;
  final String title;
  final bool showAltDesc;
  final bool editable;

  Locations(this.stream,
      {@required this.title,
      this.emptyTxt = '',
      this.onLocationTap,
      this.onDeleteAll,
      this.onDelete,
      this.showAltDesc = true,
      this.editable = true,
      this.locationIcon});

  @override
  _LocationsState createState() => _LocationsState();
}

class _LocationsState extends State<Locations> {
  Offset _tapPos;

  renderEmptyText() {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(widget.emptyTxt,
              style: TextStyle(fontSize: 20, color: Colors.grey))
        ],
      ),
    );
  }

  getLocationMenuItems(sh.Location location) {
    var items = [
      PopupMenuItem(
        value: LocationAction.details,
        child: Text('enter details'),
      )
    ];

    if (widget.onDelete != null) {
      items.add(
        PopupMenuItem(
          value: LocationAction.remove,
          child: Text('remove'),
        ),
      );
    }

    return items;
  }

  showDetailsDialog(sh.Location location) {
    final TextEditingController controller = new TextEditingController();

    controller.text = location.altDesc;

    showDialog(
        context: context,
        builder: (ctx) {
          return SimpleDialog(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    Text(location.desc),
                    TextField(
                      controller: controller,
                      decoration: InputDecoration(hintText: 'note'),
                      onSubmitted: (_) {
                        location.altDesc = controller.text;
                        widget.stream.add(widget.stream.value);
                        Navigator.pop(ctx);
                      },
                    ),
                  ],
                ),
              ),
            ],
          );
        });
  }

  onLocationLongPress(sh.Location location) async {
    var store = p.Provider.of<StoreProvider>(context);
    var overlay = Overlay.of(context).context.findRenderObject();
    var isUnique = store.isBookmarkUnique(location);

    var choice = await showMenu(
        position: RelativeRect.fromRect(
            _tapPos & Size(40, 40), Offset.zero & overlay.semanticBounds.size),
        context: context,
        items: getLocationMenuItems(location));

    switch (choice) {
      case LocationAction.remove:
        widget.onDelete(location);
        break;

      case LocationAction.details:
        showDetailsDialog(location);
        break;
    }
  }

  storeTapPosition(TapDownDetails tap) {
    _tapPos = tap.globalPosition;
  }

  showLocation(sh.Location loc) {
    var store = p.Provider.of<StoreProvider>(context);

    store.searchedLocation.add(loc);
    store.tabIdx.add(0);
  }

  renderList(AsyncSnapshot<List<sh.Location>> snapshot, StoreProvider store) {
    return GestureDetector(
      onTapDown: storeTapPosition,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: snapshot.data.length,
        itemBuilder: (ctx, idx) {
          var loc = snapshot.data[idx];

          return ListTile(
            title: Text(loc.desc),
            subtitle: loc.altDesc == null ? null : Text(loc.altDesc),
            trailing: widget.locationIcon == null
                ? sh.BookmarkButton(loc)
                : widget.locationIcon(loc),
            onLongPress: () {
              onLocationLongPress(loc);
            },
            onTap: () {
              showLocation(loc);

              if (widget.onLocationTap != null) {
                widget.onLocationTap();
              }
            },
          );
        },
      ),
    );
  }

  @override
  build(ctx) {
    var store = p.Provider.of<StoreProvider>(ctx);

    return StreamBuilder<List<sh.Location>>(
        stream: widget.stream,
        builder: (context, snapshot) {
          var noData = snapshot.data == null || snapshot.data.isEmpty;
          return Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Opacity(
                    opacity: 0,
                    child: IconButton(
                      padding: EdgeInsets.only(right: 28),
                      icon: Icon(Icons.delete),
                      onPressed: null,
                    ),
                  ),
                  Text(
                    widget.title,
                    style: TextStyle(color: Colors.grey, fontSize: 20),
                  ),
                  Opacity(
                    opacity: noData ? 0 : 1,
                    child: IconButton(
                      padding: EdgeInsets.only(right: 28),
                      icon: Icon(Icons.delete),
                      onPressed: noData ? null : widget.onDeleteAll,
                    ),
                  ),
                ],
              ),
              noData ? renderEmptyText() : renderList(snapshot, store),
            ],
          );
        });
  }
}

class Location {
  double lat;
  double lng;
  String desc;
  String altDesc;
  DateTime date;
  gc.GeocodingResult apiResult;

  Location(
      {@required this.lat,
      @required this.lng,
      this.desc,
      this.altDesc,
      this.date,
      this.apiResult});
}

class BookmarkButton extends StatelessWidget {
  final sh.Location location;

  BookmarkButton(this.location);

  @override
  build(ctx) {
    var store = p.Provider.of<StoreProvider>(ctx);

    return StreamBuilder(
      stream: store.bookmarks,
      builder: (ctx, snapshot) {
        var uniq = store.isBookmarkUnique(location);
        var onPressed = snapshot.data == null
            ? null
            : uniq
                ? () => store.addBookmark(location)
                : () => store.removeBookmark(location);
        return IconButton(
          icon: Icon(uniq ? Icons.bookmark_border : Icons.bookmark),
          onPressed: onPressed,
        );
      },
    );
  }
}
