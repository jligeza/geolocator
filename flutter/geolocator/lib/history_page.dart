import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import './store.dart' show StoreProvider;
import 'package:provider/provider.dart' as p;
import './shared.dart' as sh;

class HistoryPage extends StatelessWidget {
  @override
  build(ctx) {
    var store = p.Provider.of<StoreProvider>(ctx);

    return sh.Locations(
      store.history,
      title: 'Search history',
      onDeleteAll: store.removeHistory,
      onDelete: (location) {
        store.removeHistoryEntry(location);
      },
    );
  }
}
