import 'package:flutter/material.dart';
import 'dart:async';
import 'package:provider/provider.dart' as p;
import 'package:google_maps_webservice/geocoding.dart' as gc;
import './store.dart' show StoreProvider;
import './shared.dart' as sh;

class LocationPage extends StatefulWidget {
  @override
  _LocationPageState createState() => _LocationPageState();
}

class PositionRefreshButton extends StatefulWidget {
  final int cooldown;

  PositionRefreshButton({this.cooldown = 5000});

  @override
  _PositionRefreshButtonState createState() => _PositionRefreshButtonState();
}

class _PositionRefreshButtonState extends State<PositionRefreshButton> {
  Timer timer;
  DateTime diff;
  bool disabled = false;

  onPressed() {
    var store = p.Provider.of<StoreProvider>(context);

    store.searchLocation();

    setState(() {
      disabled = true;
    });

    timer = Timer(Duration(milliseconds: widget.cooldown), () {
      if (!mounted) return;

      setState(() {
        disabled = false;
      });
    });
  }

  @override
  build(ctx) {
    return IconButton(
      icon: Icon(Icons.refresh),
      onPressed: disabled ? null : onPressed,
    );
  }
}

class LastLocationUpdate extends StatefulWidget {
  @override
  _LastLocationUpdateState createState() => _LastLocationUpdateState();
}

class _LastLocationUpdateState extends State<LastLocationUpdate> {
  Timer timer;

  @override
  void initState() {
    super.initState();

    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (mounted) {
        setState(() {});
      } else {
        timer.cancel();
      }
    });
  }

  @override
  build(ctx) {
    var store = p.Provider.of<StoreProvider>(ctx);

    var lastUpdate = store.lastAddressUpdate.value;
    Duration diff;
    String prettyDiff;

    if (lastUpdate != null) {
      diff = DateTime.now().difference(lastUpdate);

      if (diff.inDays > 0) {
        prettyDiff =
            diff.inDays.toString() + ' day' + (diff.inDays > 1 ? 's' : '');
      } else if (diff.inHours > 0) {
        prettyDiff =
            diff.inHours.toString() + ' hour' + (diff.inHours > 1 ? 's' : '');
      } else if (diff.inMinutes > 0) {
        prettyDiff = diff.inMinutes.toString() +
            ' minute' +
            (diff.inMinutes > 1 ? 's' : '');
      } else {
        prettyDiff = diff.inSeconds.toString() + ' seconds';
      }
    }

    return StreamBuilder<DateTime>(
      stream: store.lastAddressUpdate,
      builder: (ctx, snapshot) {
        var txt;
        if (snapshot == null || snapshot.data == null || prettyDiff == null) {
          txt = Text('Last update: never');
        } else {
          txt = Text('Last update: ' + prettyDiff + ' ago');
        }
        return Padding(
          padding: const EdgeInsets.all(5),
          child: Opacity(
            opacity: 0.5,
            child: txt,
          ),
        );
      },
    );
  }
}

class _LocationPageState extends State<LocationPage> {
  bool initialized = false;

  tryInitSearchLocation() {
    if (initialized) return;

    initialized = true;

    var store = p.Provider.of<StoreProvider>(this.context);

    if (store.address.value == null) {
      store.searchLocation();
    }
  }

  renderBody(sh.Location location) {
    if (location == null) {
      return Text('Position not loaded.');
    }
    if (location.desc == null) {
      return Text('Unknown location.');
    }
    return Text(
      location.desc,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: 20),
    );
  }

  @override
  build(ctx) {
    var store = p.Provider.of<StoreProvider>(ctx);

    tryInitSearchLocation();

    return StreamBuilder<sh.Location>(
        stream: store.address,
        builder: (ctx, snapshot) {
          var location = snapshot.data;

          return Stack(alignment: Alignment.center, children: [
            Positioned(
                top: 10,
                right: 10,
                child: Row(
                  children: <Widget>[
                    PositionRefreshButton(),
                    sh.BookmarkButton(location),
                  ],
                )),
            Column(
              children: <Widget>[
                Spacer(),
                renderBody(location),
                Spacer(),
                LastLocationUpdate(),
              ],
            )
          ]);
        });
  }
}
