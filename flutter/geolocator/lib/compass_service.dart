import 'package:rxdart/rxdart.dart' as rx;
import 'package:flutter_compass/flutter_compass.dart';

class CompassService {
  static final _azimuth = rx.BehaviorSubject<double>();
  static bool _initializedAzimuth = false;

  getAzimuthStream() {
    if (!_initializedAzimuth) {
      _initializedAzimuth = true;

      FlutterCompass.events.listen((data) {
        _azimuth.add(data);
      });
    }

    return _azimuth;
  }
}
