import 'package:flutter/material.dart';
import 'package:provider/provider.dart' as p;
import './store.dart' show StoreProvider;
import './compass_page.dart' show CompassPage;
import './location_page.dart' show LocationPage;
import './bookmarks_page.dart' show BookmarksPage;
import './history_page.dart' show HistoryPage;

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with SingleTickerProviderStateMixin {
  final store = StoreProvider();
  final tabs = [
    Tab(icon: Icon(Icons.navigation)),
    Tab(icon: Icon(Icons.bookmark)),
    Tab(icon: Icon(Icons.history)),
    Tab(icon: Icon(Icons.my_location)),
    Tab(icon: Icon(Icons.person)),
  ];
  TabController tabController;

  @override
  void initState() {
    super.initState();

    tabController = TabController(vsync: this, length: tabs.length);

    store.tabIdx.listen((idx) {
      if (idx == null) return;

      tabController.animateTo(idx);
    });
  }

  @override
  build(ctx) {
    return p.MultiProvider(
        providers: [
          p.Provider<StoreProvider>.value(value: store),
        ],
        child: MaterialApp(
          home: DefaultTabController(
            length: tabs.length,
            child: Scaffold(
              appBar: AppBar(
                bottom: TabBar(
                  controller: tabController,
                  tabs: tabs,
                  onTap: (idx) {
                    store.tabIdx.add(idx);
                  },
                ),
                title: Text('Geolocator'),
              ),
              body: TabBarView(
                controller: tabController,
                children: [
                  CompassPage(),
                  BookmarksPage(),
                  HistoryPage(),
                  LocationPage(),
                  Text('todo about'),
                ],
              ),
            ),
          ),
        ));
  }
}
