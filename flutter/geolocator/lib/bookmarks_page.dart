import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import './store.dart' show StoreProvider;
import 'package:provider/provider.dart' as p;
import './shared.dart' as sh;

class BookmarksPage extends StatelessWidget {
  @override
  build(ctx) {
    var store = p.Provider.of<StoreProvider>(ctx);

    return sh.Locations(
      store.bookmarks,
      title: 'Bookmarks',
      onDeleteAll: store.removeBookmarks,
    );
  }
}
