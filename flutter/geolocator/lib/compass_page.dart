import 'package:flutter/material.dart';
import 'package:provider/provider.dart' as p;
import 'package:google_maps_webservice/geocoding.dart' as gc;
import 'package:geolocator/geolocator.dart' as gl;
import './compass_service.dart' as c;
import './helpers.dart' as h;
import './store.dart' show StoreProvider;
import './shared.dart' as sh;
import 'package:great_circle_distance2/great_circle_distance2.dart' as gcd;

Duration animInterval = Duration(milliseconds: 300);

class Compass extends StatefulWidget {
  @override
  createState() => CompassState();
}

class CompassState extends State<Compass> with TickerProviderStateMixin {
  AnimationController controller;
  Animation animation;
  double initAngle;
  final compass = c.CompassService();

  @override
  initState() {
    super.initState();

    controller = AnimationController(vsync: this, duration: animInterval);

    controller.addListener(onAnimate);

    initAngle = 0.0;

    compass.getAzimuthStream().throttleTime(animInterval).listen((angle) {
      angle = angle != null ? angle.abs() : 0;
      double nextAngle;

      double angleDiff = initAngle - angle;
      var isOverturn = angleDiff.abs() > 180;
      int angleDiffSign = initAngle - angle < 0 ? -1 : 1;
      nextAngle =
          isOverturn ? initAngle - (360 * angleDiffSign) - angle : angleDiff;

      animate(nextAngle);
      initAngle = angle;
    });
  }

  animate(angle) {
    if (!mounted) return;

    controller.reset();

    var initAngle = this.initAngle ?? 0.0;
    var curvature = CurvedAnimation(parent: controller, curve: Curves.linear);
    animation =
        Tween<double>(begin: -1 * initAngle, end: -1 * (initAngle - angle))
            .animate(curvature);

    controller.forward();
  }

  onAnimate() {
    if (!mounted) return;
    setState(() {});
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  build(ctx) {
    double angle = animation == null ? 0 : animation.value;
    double normalizedAngle =
        angle > 360 ? angle - 360 : angle < 0 ? 360 + angle : angle;

    return Stack(
        alignment: Alignment.center,
        overflow: Overflow.clip,
        children: <Widget>[
          Positioned(
            bottom: 70,
            child: Text(
              (normalizedAngle.toInt()).toString() + '°',
              style: TextStyle(color: Color.fromRGBO(0, 0, 0, 0.5)),
            ),
          ),
          Transform.scale(
            scale: 0.3,
            child: LocationPointer(null),
          ),
          Container(
            child: Transform.rotate(
                angle: h.angleToRad(angle),
                child: Image.asset('assets/compass.png')),
          ),
        ]);
  }
}

class LocationPointer extends StatelessWidget {
  final gc.Location location;
  static final geolocator = gl.Geolocator();

  LocationPointer(this.location);

  @override
  build(ctx) {
    return StreamBuilder(
        stream: geolocator.getPositionStream(),
        builder: (ctx, data) {
          return Opacity(
            opacity: location == null ? 0.1 : 1,
            child: Image.asset('assets/red_arrow.png'),
          );
        });
  }
}

class LocationSearch extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LocationSearchState();
  }
}

class LocationSearchState extends State {
  final TextEditingController _controller = new TextEditingController();
  String location;

  build(ctx) {
    var s = p.Provider.of<StoreProvider>(ctx);

    var icon;
    if (location != null && location != '') {
      icon = IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            _controller.clear();

            s.searchedLocation.add(null);

            setState(() {
              location = '';
            });
          });
    }

    return TextField(
      controller: _controller,
      onChanged: (text) {
        setState(() {
          location = text;
        });
      },
      onSubmitted: s.searchAddress,
      decoration:
          InputDecoration(hintText: 'enter location name', suffixIcon: icon),
    );
  }
}

class Distance extends StatelessWidget {
  final sh.Location location;

  Distance(this.location);

  @override
  build(ctx) {
    var store = p.Provider.of<StoreProvider>(ctx);

    return StreamBuilder<gl.Position>(
        stream: store.position,
        builder: (context, snapshot) {
          var dist;
          var km;
          var m;
          if (snapshot.data != null) {
            dist = gcd.GreatCircleDistance.fromDegrees(
                    latitude1: snapshot.data.latitude,
                    longitude1: snapshot.data.longitude,
                    latitude2: location.lat,
                    longitude2: location.lng)
                .haversineDistance();
            km = (dist / 1000).floor();
            m = (dist - km * 1000).floor();
          }
          return Text(
            dist == null
                ? 'Distance not available.'
                : 'Distance: ' + (km == 0 ? '' : '$km km') + ' $m m',
            style: TextStyle(color: Colors.grey),
          );
        });
  }
}

class CompassPage extends StatelessWidget {
  @override
  build(ctx) {
    var store = p.Provider.of<StoreProvider>(ctx);

    return Container(
        padding: EdgeInsets.all(10),
        alignment: Alignment.center,
        child: Column(
          children: <Widget>[
            LocationSearch(),
            Container(
              padding: EdgeInsets.only(top: 20, bottom: 20),
              child: StreamBuilder<sh.Location>(
                stream: store.searchedLocation,
                builder: (ctx, val) {
                  if (val == null || val.data == null) return Text('');
                  if (val.data.lat == null) return Text('location not found');

                  var loc = val.data;

                  return Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              loc.desc,
                              softWrap: true,
                              textScaleFactor: 1.5,
                            ),
                          ),
                          sh.BookmarkButton(loc),
                        ],
                      ),
                      Container(
                        child: Distance(loc),
                        width: double.infinity,
                      ),
                    ],
                  );
                },
              ),
            ),
            Spacer(),
            Container(height: 300, child: Compass()),
          ],
        ));
  }
}
