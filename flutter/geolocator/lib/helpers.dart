import 'package:rxdart/rxdart.dart' as rx;
import 'package:geolocator/geolocator.dart' as gl;
import 'package:google_maps_webservice/geocoding.dart' as gc;
import 'dart:math' as math;

var geolocator = gl.Geolocator();

rx.BehaviorSubject<gl.Position> getLocationSubject() {
  var subject = rx.BehaviorSubject<gl.Position>();

  subject.addStream(geolocator.getPositionStream());

  return subject;
}

String getLocationText(gc.GeocodingResponse res) {
  return res.results[0]?.formattedAddress;
}

double angleToRad(double angle) => angle * math.pi / 180;
