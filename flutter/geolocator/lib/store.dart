import 'package:rxdart/rxdart.dart' as rx;
import 'package:google_maps_webservice/geocoding.dart' as gc;
import './helpers.dart' as h;
import './geocoding.dart';
import './shared.dart' as sh;

class StoreProvider {
  final searchedLocation = rx.BehaviorSubject<sh.Location>();
  final position = h.getLocationSubject().throttleTime(Duration(seconds: 1));
  final address = rx.BehaviorSubject<sh.Location>();
  final addressError = rx.BehaviorSubject();
  final lastAddressUpdate = rx.BehaviorSubject<DateTime>();
  final bookmarks = rx.BehaviorSubject.seeded(<sh.Location>[]);
  final tabIdx = rx.BehaviorSubject.seeded(0);
  final history = rx.BehaviorSubject.seeded(<sh.Location>[]);

  addBookmark(sh.Location location) {
    bookmarks.value.insert(0, location);

    bookmarks.add(bookmarks.value);
  }

  removeBookmark(sh.Location location) {
    bookmarks.value.removeWhere((item) => item == location);

    bookmarks.add(bookmarks.value);
  }

  removeBookmarks() {
    bookmarks.add([]);
  }

  removeHistory() {
    history.add([]);
  }

  removeHistoryEntry(sh.Location location) {
    history.value.removeWhere((item) => item == location);

    history.add(history.value);
  }

  isBookmarkUnique(sh.Location location) {
    return !bookmarks.value.any((item) => item == location);
  }

  addHistory(sh.Location location) {
    history.value.insert(0, location);

    history.add(history.value);
  }

  searchAddress(String val) async {
    if (val == '' || val == null) return;

    var res = await geocoding.searchByAddress(val);
    var loc = res.results.isEmpty
        ? sh.Location(
            lat: null,
            lng: null,
            desc: null,
            date: DateTime.now(),
          )
        : sh.Location(
            lat: res.results[0].geometry.location.lat,
            lng: res.results[0].geometry.location.lng,
            desc: res.results[0].formattedAddress,
            date: DateTime.now(),
            apiResult: res.results[0]);

    searchedLocation.add(loc);

    if (loc != null) {
      addHistory(loc);
    }
  }

  searchLocation() async {
    var pos = await position.first;

    if (pos == null) return;

    try {
      var res = await geocoding
          .searchByLocation(gc.Location(pos.latitude, pos.longitude));

      if (res.results.isNotEmpty) {
        var first = res.results[0];

        address.add(sh.Location(
            lat: pos.latitude,
            lng: pos.longitude,
            desc: first.formattedAddress,
            date: DateTime.now(),
            apiResult: first));
      } else {
        address.add(sh.Location(
          lat: pos.latitude,
          lng: pos.longitude,
          date: DateTime.now(),
        ));
      }
      lastAddressUpdate.add(DateTime.now());

      if (await addressError.first != null) {
        addressError.add(null);
      }
    } catch (e) {
      addressError.add(e);
    }
  }

  dispose() {
    history.close();
    tabIdx.close();
    searchedLocation.close();
  }
}
